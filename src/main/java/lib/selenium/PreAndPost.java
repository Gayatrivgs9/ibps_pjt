package lib.selenium;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import com.ibps.pages.LoginPage;

import lib.utils.DataInputProvider;

public class PreAndPost extends WebDriverServiceImpl{

	public String dataSheetName, sheetName;
	public static int rowNumC1=0, rowNumC2=0, rowNumC3=0, rowNumC4=0, rowNumC5 =0;

	@BeforeSuite
	public void beforeSuite() throws InterruptedException, IOException {
		startReport();
	}

	@BeforeClass
	public void beforeClass() throws InterruptedException, IOException {
		startTestCase(testCaseName, testDescription);
		startApp("chrome"); 
		new LoginPage(driver, test)
		.startSBILogin()
		/*// Bnp candidate
		.enterRegistrationNumber("88000120")
		.enterPassword("H433KLYD")*/
		/*// Nehru candidate 
		.enterRegistrationNumber("96000008")
		.enterPassword("V373EPBC")*/
		/*.enterRegistrationNumber("7000021")
		.enterPassword("PD884BWR")*/
		//meghalaya 
		.enterRegistrationNumber("1830000003")
		.enterPassword("3C85VUCL")
		.clickSubmit() 
		.clickBasicNextBtn() 
		.clickPhotoNextBtn();
	}

	@BeforeMethod
	public void beforeMethod() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		startTestModule(nodes);
		test.assignAuthor(authors);
		test.assignCategory(category);
		//startApp("chrome");
	}

	@AfterMethod
	public void afterMethod() {		
		endResult();
	}

	@AfterClass
	public void afterClass() {
		closeAllBrowsers();
	}
	@AfterSuite
	public void afterSuite() {
	}

	@DataProvider(name="fetchData") //,indices= {0,1})       
	public  Object[][] getData(){		  
		return DataInputProvider.getSheet(dataSheetName, sheetName);		
	}

}
