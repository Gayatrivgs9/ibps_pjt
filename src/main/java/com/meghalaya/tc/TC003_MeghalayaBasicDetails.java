package com.meghalaya.tc;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ibps.pages.PersonalDetailsPage;

import lib.selenium.PreAndPost;

public class TC003_MeghalayaBasicDetails extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC003_MeghalayaBasicDetails";
		testDescription = "Meghalaya age validation";
		nodes = "Meghalaya";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "Meghalaya_3";
		sheetName = "MeghalayaUsers3";
	}

	@Test(dataProvider = "fetchData")	
	public void basicDetails(String sNum, String post,String position, String location, String category, String subCategory ,
			String pwd, String	pwdType, String	pwdPercentage, String region,String aadhaar, String meghalaya, String examCenter,

			String maxDob, String minDob, String dateAsOnDate, String dateAsOnDateCal, String dateAsOnDate2,String dateLeap,
			String MaxAgeMOne, String MaxAge,String MaxAgePOne, String MinAgeMOne,String MinAge, String MinAgePOne, String MaxAgeP45,
			String MinAgeM45, String leapPass, String asOnDateMaxResult,String asOnDateMinResult, String payamount) throws Exception {

		new PersonalDetailsPage(driver, test)
		.selectDropDownUsingVisibleTextNew(locateElement("selectPost_id"), post) 
		.selectDropDownUsingVisibleTextNew(locateElement("selectPosition_id"), position)
		.selectDropDownUsingVisibleTextNew(locateElement("selectLocation_id"), location)
		.radioButtonId("IBPS_category_id", category) 
		.alertAccept()
		.selectDropDownUsingIndexNew(locateElement("selectSubCat_id"), subCategory)
		.radioButtonId("IBPS_disablilty_id", pwd)
		.selectDropDownUsingIndexNew(locateElement("IBPS_DisabilityType_id"), pwdType)
		//.alertAccept() 
		.selectDropDownUsingIndexNew(locateElement("IBPS_disabliltyPercent_id"), pwdPercentage)
		.selectDropDownUsingIndexNew(locateElement("IBPS_religion_name"), region)
		.selectDropDownUsingIndexNew(locateElement("IBPS_optaadhar_id"), aadhaar)
		.radioButtonId("meghalayaResident_id", meghalaya)
		//.selectDropDownUsingIndexNew(locateElement("IBPS_selstateapplied_id"), state)
		.selectDropDownUsingIndexNew(locateElement("IBPS_selexamcentre_id"), examCenter)
		.rowNumC3()		
		.ageValidatorMax(rowNumC3, maxDob, "Meghalaya_3", "MeghalayaUsers3")
		.ageValidatorMin(rowNumC3, minDob, "Meghalaya_3", "MeghalayaUsers3")
		.ageValidatorAsOnDate(rowNumC3,dateAsOnDate2, "Meghalaya_3", "MeghalayaUsers3");
		//.ageValidatorLeapyear(dateLeap, "BNP_1", "BnpUsers1")
		//.clickDetailsButton();


	}
}








