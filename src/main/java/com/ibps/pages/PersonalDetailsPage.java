package com.ibps.pages;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class PersonalDetailsPage extends PreAndPost{

	public PersonalDetailsPage(RemoteWebDriver driver, ExtentTest test) {
		//PageFactory.initElements(driver, this);
		this.driver = driver;
		this.test = test; 
	}

	public PersonalDetailsPage rowNumC1() {
		rowNumC1 = rowNumC1+1;
		return this; 
	}
	public PersonalDetailsPage rowNumC2() {
		rowNumC2 = rowNumC2+1;
		return this; 
	}
	public PersonalDetailsPage rowNumC3() {
		rowNumC3 = rowNumC3+1;
		return this; 
	}
	public PersonalDetailsPage rowNumC4() {
		rowNumC4 = rowNumC4+1;
		return this; 
	}
	public PersonalDetailsPage rowNumC5() {
		rowNumC5 = rowNumC5+1;
		return this; 
	}
	public PersonalDetailsPage radioButtonId(String id, String selectableValue) throws InterruptedException {

		if (!selectableValue.equalsIgnoreCase("N/A")) {
			List<WebElement> radio = driver.findElements(By.id(prop.getProperty(id)));
			for (WebElement webElement : radio) { 
				if (webElement.getAttribute("value").trim().equals(selectableValue)) {
					webElement.click();
					break;
				}
			}
		} 
		return this;
	}
	public PersonalDetailsPage dateSelection1(String id, String date) throws Exception {
		if(!date.equals("N/A"))	{
			String[] dat = date.split("-");
			List<String> monthList = Arrays.asList("January", "February", "March", "April", "May", "June", "July", "August",
					"September", "October", "November", "December");
			String[] ids = id.split("-");
			selectDropdown(ids[0], "Day");
			selectDropdown(ids[1], "Month");
			driver.findElement(By.id(ids[2])).sendKeys("Year");
			selectDropdown(ids[0], dat[0]);
			selectDropdown(ids[1], monthList.get(Integer.parseInt(dat[1]) - 1));
			driver.findElement(By.id(ids[2])).sendKeys(dat[2]);
		}
		return this;
	}
	public PersonalDetailsPage radioButtonName(String name, String selectableValue) {
		if (!selectableValue.equalsIgnoreCase("N/A")) {
			List<WebElement> radio = driver.findElements(By.name(prop.getProperty(name)));
			for (WebElement webElement : radio) {
				if (webElement.getAttribute("value").trim().equals(selectableValue)) {
					webElement.click();
					break;
				}
			}
		}
		return this;
	}
	public PersonalDetailsPage enterPost(String data) {
		selectDropDownUsingIndex(locateElement("name", "selpost_name"), Integer.parseInt(data));
		return this;
	}
	public PersonalDetailsPage clickCategory(String data) throws InterruptedException {
		System.out.println(data);
		if(data.equalsIgnoreCase("UR")) {
			click(locateElement("xpath", "eleCategory_UR_xpath")); 
		} else if (data.equalsIgnoreCase("GEN")) {
			System.err.println("Selected Category");
			click(locateElement("xpath", "eleCategory_GENERAL_xpath"));	
		}else if (data.equalsIgnoreCase("EWS")) {
			click(locateElement("xpath", "eleCategory_EWS_xpath"));	
		}else if (data.equalsIgnoreCase("OBC")) {
			click(locateElement("xpath", "eleCategory_OBC_xpath"));	
		}else if (data.equalsIgnoreCase("ST")) {
			click(locateElement("xpath", "eleCategory_ST_xpath"));	
		}else if (data.equalsIgnoreCase("SC")) {
			click(locateElement("xpath", "eleCategory_SC_xpath"));	
		} else {
			System.out.println("Category not matched");
		}
		//Thread.sleep(1000);
		alertMsg();
		//acceptAlert();
		return this;
	}
	public PersonalDetailsPage clickCategory1(String data) throws InterruptedException {
		System.out.println(data);
		List<WebElement> radio = driver.findElements(By.id("opt_cat"));
		for (WebElement webElement : radio) {
			if (webElement.getAttribute("value").trim().equals(data)) {
				webElement.click();
				break;
			}
		}
		//Thread.sleep(1000);
		alertMsg();
		//acceptAlert();
		return this;
	}
	public void radioButtonId1(String id, String selectableValue) {
		if (!selectableValue.equalsIgnoreCase("N/A")) {
			List<WebElement> radio = driver.findElements(By.id(id));
			for (WebElement webElement : radio) {
				if (webElement.getAttribute("value").trim().equals(selectableValue)) {
					webElement.click();
					break;
				}
			}
		}
	}
	public PersonalDetailsPage alertAccept() throws InterruptedException {
		String text = "";		
		try {
			Alert alert = driver.switchTo().alert();
			text = alert.getText();
			alert.accept();
			Thread.sleep(1000); 
			reportStep("The alert "+text+" is accepted.","PASS");
		} catch (NoAlertPresentException e) {
			reportStep("There is no alert present.","FAIL");
		} catch (WebDriverException e) {
			reportStep("WebDriverException : "+e.getMessage(), "FAIL");
		}  
		return this;
	}
	public PersonalDetailsPage ageValidatorMax(int rowNumC, String givenDate, String dataSheet,  String sheet) throws Exception {
		agevalidator_max(rowNumC, "IBPS_personalDOB_id", givenDate,dataSheet, sheet);
		return this; 
	}
	public PersonalDetailsPage ageValidatorMin(int rowNumC, String givenDate, String dataSheet, String sheet) throws Exception {
		agevalidator_min(rowNumC, "IBPS_personalDOB_id", givenDate, dataSheet, sheet);
		return this; 
	}
	public PersonalDetailsPage ageValidatorLeapyear(int rowNumC, String givenDate, String dataSheet, String sheet) throws Exception {
		agevalidator_leapyear(rowNumC, "IBPS_personalDOB_id", givenDate, dataSheet, sheet);
		Thread.sleep(1000); 
		return this; 
	}
	public PersonalDetailsPage ageValidatorAsOnDate(int rowNumC,String givenDate, String dataSheet, String sheet) throws Exception {
		agevalidator_asondate(rowNumC, "IBPS_personalDOB_id", givenDate, dataSheet, sheet);
		return this;  
	}

	public PersonalDetailsPage clickDetailsButton() throws InterruptedException {
		Thread.sleep(2000);  
		click(locateElement("xpath", "details_xpath_button"));
		return this;
	}
	public PersonalDetailsPage selectDropDownUsingIndexNew(WebElement ele, String index) {
		try {
			if (!index.equalsIgnoreCase("N/A")) {
				int indvalue=Integer.parseInt(index);
				new Select(ele).selectByIndex(indvalue);
				reportStep("The dropdown is selected with index "+index,"PASS");
			} }catch (WebDriverException e) {
				reportStep("The element: "+ele+" could not be found.", "FAIL");
			} 
		return this;
	} 
	public PersonalDetailsPage selectDropDownUsingVisibleTextNew(WebElement ele, String value) {
		try {
			new Select(ele).selectByVisibleText(value);
			reportStep("The dropdown is selected with text "+value,"PASS");
		}catch (WebDriverException e) {
			reportStep("The element: "+ele+" could not be found.", "FAIL");
		} 
		return this;
	} 

	public PersonalDetailsPage clickNew(WebElement ele) throws InterruptedException {
		String text = "";
		try {
			/*Thread.sleep(500);
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));*/			
			text = ele.getText();
			ele.click();
			reportStep("The element "+text+" is clicked", "PASS");
		} catch (InvalidElementStateException e) {
			reportStep("The element: "+text+" could not be clicked", "FAIL");
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		} 
		return this; 
	}
	public PersonalDetailsPage inputTextId(String id, String inputText) throws Exception {
		Thread.sleep(2000); 
		if (!inputText.equalsIgnoreCase("N/A")) {
			driver.findElement(By.id(prop.getProperty(id))).clear();
			driver.findElement(By.id(prop.getProperty(id))).sendKeys(inputText.trim());
		} 
		return this; 
	}
	public PersonalDetailsPage clickButtonNew(String xPath) throws Exception{
		//alert();
		driver.findElement(By.xpath(prop.getProperty(xPath))).click(); 
		waitUntilPageloads();
		return this;  
	}
	



}
