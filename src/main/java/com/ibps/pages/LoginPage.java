package com.ibps.pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class LoginPage extends PreAndPost{

	public LoginPage(RemoteWebDriver driver, ExtentTest test) {
		//PageFactory.initElements(driver, this);
				this.driver = driver;
				this.test = test;
	}
	
	@FindBy(how=How.LINK_TEXT, using="Click here for New Registration") WebElement eleNewReg;
	@FindBy(how=How.ID, using="txtregno") WebElement eleRegNum;
	@FindBy(how=How.ID, using="txtpass") WebElement elePassword;
	@FindBy(how=How.ID, using="Submit") WebElement eleSubmit;
	
	public LoginPage startSBILogin() { 
		// SBI
		//loadUrl("http://demo.sifyitest.com/raja/sbijascapr19/");
		
		//loadUrl("http://demo.sifyitest.com/premsingh/epfssaojun19/"); 
		// BNP
		//loadUrl("http://regqc.sifyitest.com/bnpdwvpjun19/");
		//loadUrl("http://regqc.sifyitest.com/nyksvpsjun19/index.php"); 
		// Nehru
		//loadUrl("http://regqc.sifyitest.com/nyksvpsjun19/"); 
		// OBC
		//loadUrl("https://regqc.sifyitest.com/obcsplojul19/"); 
		//Mint
		//loadUrl("http://regqc.sifyitest.com/igmmumvsep19/"); 
		//RBI
		//loadUrl("http://regqc.sifyitest.com/rbispvpsep19/");
		//Meghalaya 
		//loadUrl("http://regqc.sifyitest.com/rbiasstnov19/");
		//Nabard
		loadUrl("http://regqc.sifyitest.com/nabaoasdec19/"); 
		return this; 
	}
	
	public LoginPage enterRegistrationNumber(String data) throws IOException {
		String readData = readData(1, 0, "newSheetname","");
		typer(locateElement("basicInfo_id_txtregno"), data); 
		System.out.println(readData);
		return this;
	} 
	public LoginPage enterPassword(String data) throws IOException {
		String readData = readData(2, 0, "newSheetname",""); 
		typer(locateElement("basicInfo_id_txtpass"), data);  
		System.out.println(readData);
		return this; 
	}
	public LoginPage clickSubmit() throws IOException, InterruptedException {
		click(locateElement("basicInfo_id_submit"));  
		return this;  
	} 
	public LoginPage clickBasicNextBtn() throws IOException, InterruptedException {
		click(locateElement("xpath","eleBasicNextBtn_xpath"));  
		return this;  
	}
	public LoginPage clickPhotoNextBtn() throws IOException, InterruptedException {
		click(locateElement("id","basicInfo_id_submit"));  
		return this;  
	}
	
	
}











