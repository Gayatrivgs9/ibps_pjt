package com.bnp.tc;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ibps.pages.PersonalDetailsPage;

import lib.selenium.PreAndPost;

public class TC005_BnpCBasicDetails extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC005_BnpCBasicDetails";
		testDescription = "Bnp age validation";
		nodes = "BNP";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "BNP_5";
		sheetName = "BnpUsers5";
	}
	
	@Test(dataProvider = "fetchData")	
	public void basicDetails(String desc, String post,String category, String pwd, String pwdType,String compensatory, 
			String palcyCompensatoryTime,String dominantHand, String dominantHandComTime,String scribe, 
			String jammuKashmir, String region, String minority,String exServiceman, String dis_Ex_Serviceman,
			String servicePeriod, String deptCandidate, String state, String examCenter,
			
			String maxDob, String minDob, String dateAsOnDate, String dateAsOnDateCal, String dateAsOnDate2,
			String dateLeap, String MaxAgeMOne, String MaxAge,String MaxAgePOne, String MinAgeMOne,
			String MinAge, String MinAgePOne, String MaxAgeP45, String MinAgeM45, String leapPass, String asOnDateMaxResult,
			String asOnDateMinResult, String payamount) throws Exception {


		new PersonalDetailsPage(driver, test)
		.enterPost(post)
		.clickCategory(category)
		.radioButtonId("disablilty_id", pwd)
		.radioButtonName("disabliltyType_name", pwdType)
		.radioButtonName("IBPS_cerebralPalsy_name", compensatory)
		.radioButtonId("compensatory_id", palcyCompensatoryTime)
		.radioButtonName("dominantHand_name", dominantHand)  
		.radioButtonName("dominantHandCompensatoryTime_name", dominantHandComTime) 
		.radioButtonName("scribeService_name", scribe) 
		.radioButtonName("IBPS_domiciledInJammu_name", jammuKashmir)
		.inputTextId("religion_id", region)
		.radioButtonName("religiousMinority_name", minority) 
		.radioButtonName("exServiceman_name", exServiceman)
		.radioButtonName("disabledexServiceman_name", dis_Ex_Serviceman)
		.inputTextId("exServicePeriod_id", servicePeriod) 
		.radioButtonName("IBPS_ONGC_DepartmentalCanditates_name", deptCandidate) 
		.selectDropDownUsingIndexNew(locateElement("selstateapplied_id"), state)
		.selectDropDownUsingIndexNew(locateElement("selexamcentre_id"), examCenter)
		.rowNumC5()
		.ageValidatorMax(rowNumC5, maxDob, "BNP_5", "BnpUsers5")
		.ageValidatorMin(rowNumC5, minDob, "BNP_5", "BnpUsers5") 
		.ageValidatorAsOnDate(rowNumC5, dateAsOnDate, "BNP_5", "BnpUsers5")
		//.ageValidatorLeapyear(dateAsOnDate, "BnpUsers5");		
		.clickDetailsButton();
		





	}
}
