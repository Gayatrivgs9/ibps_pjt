package com.bnp.tc;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ibps.pages.PersonalDetailsPage;

import lib.selenium.PreAndPost;

public class TC001_BnpCBasicDetails extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC001_BnpCBasicDetails";
		testDescription = "Bnp age validation";
		nodes = "BNP";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "BNP_1";
		sheetName = "BnpUsers1";
	}

	@Test(dataProvider = "fetchData")	
	public void basicDetails(String desc, String post,String category, String pwd, String pwdType,String compensatory, 
			String palcyCompensatoryTime,String dominantHand, String dominantHandComTime,String scribe, 
			String jammuKashmir, String region, String minority,String exServiceman, String dis_Ex_Serviceman,
			String servicePeriod, String deptCandidate, String state, String examCenter,

			String maxDob, String minDob, String dateAsOnDate, String dateAsOnDateCal, String dateAsOnDate2,
			String dateLeap, String MaxAgeMOne, String MaxAge,String MaxAgePOne, String MinAgeMOne,
			String MinAge, String MinAgePOne, String MaxAgeP45, String MinAgeM45, String leapPass, String asOnDateMaxResult,
			String asOnDateMinResult, String payamount) throws Exception {


		new PersonalDetailsPage(driver, test)
		.enterPost(post)
		.clickCategory(category)
		.radioButtonId("disablilty_id", pwd)
		.radioButtonName("disabliltyType_name", pwdType)
		.radioButtonName("IBPS_cerebralPalsy_name", compensatory)
		.radioButtonId("compensatory_id", palcyCompensatoryTime)
		.radioButtonName("dominantHand_name", dominantHand)  
		.radioButtonName("dominantHandCompensatoryTime_name", dominantHandComTime) 
		.radioButtonName("scribeService_name", scribe) 
		.radioButtonName("IBPS_domiciledInJammu_name", jammuKashmir)
		.inputTextId("religion_id", region)
		.radioButtonName("religiousMinority_name", minority) 
		.radioButtonName("exServiceman_name", exServiceman)
		.radioButtonName("disabledexServiceman_name", dis_Ex_Serviceman)
		.inputTextId("exServicePeriod_id", servicePeriod) 
		.radioButtonName("IBPS_ONGC_DepartmentalCanditates_name", deptCandidate) 
		.selectDropDownUsingIndexNew(locateElement("selstateapplied_id"), state)
		.selectDropDownUsingIndexNew(locateElement("selexamcentre_id"), examCenter)
		.rowNumC1()		
		.ageValidatorMax(rowNumC1, maxDob, "BNP_1", "BnpUsers1")
		.ageValidatorMin(rowNumC1, minDob, "BNP_1", "BnpUsers1")
		.ageValidatorAsOnDate(rowNumC1,dateAsOnDate2, "BNP_1", "BnpUsers1")
		//.ageValidatorLeapyear(dateLeap, "BNP_1", "BnpUsers1")
		.clickDetailsButton();






	}
}









