package com.OBC.tc;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ibps.pages.PersonalDetailsPage;

import lib.selenium.PreAndPost;

public class TC003_OBCBasicDetails extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC003_OBCBasicDetails";
		testDescription = "OBC age validation";
		nodes = "OBC";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "OBC_3";
		sheetName = "OBCUsers3";
	}
	
	@Test(dataProvider = "fetchData")	
	public void basicDetails(String desc,String post,String category, String pwd, String pwdType, String compensatory, String cereberalPolicy, 
			String PalcyCompensatoryTime, String dominantHand, String	dominantHandComTime, String	scribe, String riots, 
			String JammuKashmir, String reMarried, String region,String minority, String exServiceman,
			String ServicePeriod, String CivilsideJob, String state,
			
			String maxDob, String minDob, String dateAsOnDate, String dateAsOnDateCal, String dateAsOnDate2,
			String dateLeap, String MaxAgeMOne, String MaxAge,String MaxAgePOne, String MinAgeMOne,
			String MinAge, String MinAgePOne, String MaxAgeP45, String MinAgeM45, String leapPass, String asOnDateMaxResult,
			String asOnDateMinResult, String payamount) throws Exception {


		new PersonalDetailsPage(driver, test)
		.enterPost(post)
		.clickCategory(category)
		.radioButtonId("disablilty_id", pwd)
		.radioButtonName("disabliltyType_name", pwdType)
		.radioButtonName("IBPS_Compensatorytime_id", compensatory)
		.radioButtonName("IBPS_cerebralPalsy_name", cereberalPolicy)
		.radioButtonId("compensatory_id", PalcyCompensatoryTime)
		.radioButtonName("dominantHand_name", dominantHand)  
		.radioButtonName("dominantHandCompensatoryTime_name", dominantHandComTime) 
		.radioButtonName("scribeService_name", scribe) 
		.radioButtonName("IBPS_members1984_name", riots)
		.radioButtonName("IBPS_domiciledInJammu_name", JammuKashmir)
		.radioButtonName("IBPS_relax_remarried_name", reMarried)
		.inputTextId("religion_id", region)
		.radioButtonName("IBPS_religiousMinority_name", minority)
		.radioButtonName("exServiceman_name", exServiceman)
		.inputTextId("exServicePeriod_id", ServicePeriod)  
		.radioButtonName("govJobexService_name", CivilsideJob)  
		.selectDropDownUsingIndexNew(locateElement("IBPS_selexamcentre1_id"), state)
		.rowNumC3()		
		.ageValidatorMax(rowNumC3, maxDob, "OBC_3", "OBCUsers3")
		.ageValidatorMin(rowNumC3, minDob, "OBC_3", "OBCUsers3")
		.ageValidatorAsOnDate(rowNumC3,dateAsOnDate2, "OBC_3", "OBCUsers3")
		//.ageValidatorLeapyear(dateLeap, "BNP_1", "BnpUsers1")
		.clickDetailsButton();
		

	}
}








