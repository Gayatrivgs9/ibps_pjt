package com.rbi.tc;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ibps.pages.PersonalDetailsPage;

import lib.selenium.PreAndPost;

public class TC005_RBIBasicDetails extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC005_RBIBasicDetails";
		testDescription = "RBI age validation";
		nodes = "RBI";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "RBI_5";
		sheetName = "RBIUsers5";
	}

	@Test(dataProvider = "fetchData")
	public void basicDetails(String sNum, String DESC,String Category, String PWD, String PWDTYPE ,String PersonVI, String PersonHI,
	  String CompTime, String cereberalPolicy,String compensatoryTime,String dominantHand ,String dominantHandComTime, String SCRIBE,
	  String exserviceman,String DisabledExserviceman,
	  String ServicePeriod,String defenceservice,String defenceValue, String DependentofServicemankilled,String CivilSide,
	  String MatriculateExServicemencandidate, String JammuKashmir, String reMarried, String RBIStaff,String Empid,
	  String ExEmployees,String YearofExperience,String RELIG,String MINORITY,String SelectOffice,String PriliminaryExamState,
	  String PriliminaryExamCenter,String MainExamCenter,
	  String Centerofmainexam,String ProficientLanguage,String Language,
	 
	  String maxDob, String minDob, String dateAsOnDate, String dateAsOnDateCal, String dateAsOnDate2,
	  String dateLeap, String MaxAgeMOne, String MaxAge,String MaxAgePOne, String MinAgeMOne,
	  String MinAge, String MinAgePOne, String MaxAgeP45, String MinAgeM45, String leapPass, String asOnDateMaxResult,
	  String asOnDateMinResult, String payamount) throws Exception {


	 new PersonalDetailsPage(driver, test)
	 //.detailsClick()
	 .clickDetailsButton()
	 //.enterPost(post)
	 .clickCategory1(Category)
	 
	 .radioButtonId("IBPS_disablilty_id", PWD)
	 .selectDropDownUsingIndexNew(locateElement("IBPS_disabliltyType_name"), PWDTYPE)
	 .radioButtonName("IBPS_personVI_name", PersonVI)
	 .radioButtonName("IBPS_personHI_name", PersonHI)
	 .radioButtonId("IBPS_compTime_name", CompTime)
	 .radioButtonId("IBPS_cerebralPalsy_name", cereberalPolicy)
	 .radioButtonId("compensatory_id", compensatoryTime)
	 .radioButtonName("IBPS_dominantHand_name", dominantHand)
	 .radioButtonName("IBPS_dominantHandCompensatoryTime_name", dominantHandComTime)
	 .radioButtonName("scribeService_name", SCRIBE)
	 .radioButtonName("IBPS_exServiceman_name", exserviceman)
	 .radioButtonId("disabledexServiceman_name", DisabledExserviceman)
	 .inputTextId("IBPS_exServicePeriod_id", ServicePeriod)
	 //.dateSelection1("IBPS_defenceService_name", defenceservice)
	 .selectDropDownUsingIndexNew(locateElement("IBPS_defenceService_day"),defenceValue)
	 .selectDropDownUsingIndexNew(locateElement("IBPS_defenceService_month"),defenceValue)
	 .selectDropDownUsingIndexNew(locateElement("IBPS_defenceService_year"),defenceValue)
	 .radioButtonName("depservice_name", DependentofServicemankilled)
	 .radioButtonName("govJobexService_name", CivilSide)
	 .radioButtonName("IBPS_matricExservice_name", MatriculateExServicemencandidate)
	 .radioButtonName("IBPS_domiciledInJammu_name", JammuKashmir)
	 .radioButtonId("IBPS_remarried_name", reMarried)
	 .radioButtonName("IBPS_rbiStaff_name", RBIStaff)
	 .inputTextId("IBPS_pfIndex_name", Empid)
	 .radioButtonId("IBPS_exRbi_name", ExEmployees)
	 .inputTextId("IBPS_yearofexp_name", YearofExperience)
	 .selectDropDownUsingIndexNew(locateElement("IBPS_religion_id"),RELIG)
	 .radioButtonName("IBPS_minority_id",MINORITY)
	 .selectDropDownUsingIndexNew(locateElement("IBPS_seloffice_id"),SelectOffice)
	 .selectDropDownUsingIndexNew(locateElement("IBPS_preExamState_name"),PriliminaryExamState)
	 .selectDropDownUsingIndexNew(locateElement("IBPS_preExamCentre_name"),PriliminaryExamCenter)
	 .selectDropDownUsingIndexNew(locateElement("IBPS_mainExamState_name"),MainExamCenter)
	 .selectDropDownUsingIndexNew(locateElement("IBPS_mainExamcentre_name"),Centerofmainexam)
	 .radioButtonId("IBPS_proficientlang_name", ProficientLanguage)
	 .selectDropDownUsingIndexNew(locateElement("IBPS_statelang"), Language)
		.rowNumC5()		
		.ageValidatorMax(rowNumC5, maxDob, "RBI_5", "RBIUsers5")
		.ageValidatorMin(rowNumC5, minDob, "RBI_5", "RBIUsers5")
		.ageValidatorAsOnDate(rowNumC5,dateAsOnDate2, "RBI_5", "RBIUsers5")
		//.ageValidatorLeapyear(dateLeap, "BNP_1", "BnpUsers1")
		.clickDetailsButton();


	}
}








