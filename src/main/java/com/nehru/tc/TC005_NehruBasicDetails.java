package com.nehru.tc;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ibps.pages.PersonalDetailsPage;

import lib.selenium.PreAndPost;

public class TC005_NehruBasicDetails extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC005_NehruBasicDetails";
		testDescription = "Nehru age validation";
		nodes = "Nehru";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "Nehru_5";
		sheetName = "NehruUsers5";
	}
	
	@Test(dataProvider = "fetchData")	
	public void basicDetails(String post,String category, String pwd, String pwdType ,String cerebralPalcy, String	PalcyCompensatoryTime,	
			String dominantHand, String	dominantHandComTime, String	SCRIBE,  String region,String MINORITY,String exServiceman,
			String dis_Ex_Serviceman, String ServicePeriod, String CivilsideJob, String ServiceDependant,String DeptCandidate,
			String GovtServant,String MerriageRelax,String	Sportsperson,String	JammuKashmir,String MoreThanOneWife, String state, String examCenter,
			
			String maxDob, String minDob, String dateAsOnDate, String dateAsOnDateCal, String dateAsOnDate2,
			String dateLeap, String MaxAgeMOne, String MaxAge,String MaxAgePOne, String MinAgeMOne,
			String MinAge, String MinAgePOne, String MaxAgeP45, String MinAgeM45, String leapPass, String asOnDateMaxResult,
			String asOnDateMinResult, String payamount) throws Exception {

		new PersonalDetailsPage(driver, test)
		.enterPost(post)
		.clickCategory(category)
		.radioButtonId("disablilty_id", pwd)
		.radioButtonName("disabliltyType_name", pwdType)
		.radioButtonId("disabilitysuffersoc1_id", cerebralPalcy)
		.radioButtonId("compensatory_id", PalcyCompensatoryTime)
		.radioButtonName("dominantHand_name", dominantHand)  
		.radioButtonName("dominantHandCompensatoryTime_name", dominantHandComTime) 
		.radioButtonName("scribeService_name", SCRIBE) 
		.inputTextId("religion_id", region)
		.radioButtonName("religiousMinority_name", MINORITY) 
		.radioButtonName("exServiceman_name", exServiceman)
		.radioButtonName("disabledexServiceman_name", dis_Ex_Serviceman)
		.inputTextId("exServicePeriod_id", ServicePeriod)  
		.radioButtonName("govJobexService_name", CivilsideJob) 
		.radioButtonName("depservice_name", ServiceDependant)
		.radioButtonName("IBPS_ONGC_DepartmentalCanditates_name", DeptCandidate) 
		.radioButtonName("IBPS_ONGC_GovtServent_name", GovtServant) 
		.radioButtonName("IBPS_relax_remarried_name", MerriageRelax)
		.radioButtonName("IBPS_merit_person_name", Sportsperson)
		.radioButtonName("IBPS_domiciledInJammu_name", JammuKashmir)
		.radioButtonName("morethan_one_wife_name", MoreThanOneWife)
		.selectDropDownUsingIndexNew(locateElement("selstateapplied_id"), state)
		.selectDropDownUsingIndexNew(locateElement("selexamcentre_id"), examCenter)
		.rowNumC5()		
		.ageValidatorMax(rowNumC5, maxDob, "Nehru_5", "NehruUsers5")
		.ageValidatorMin(rowNumC5, minDob, "Nehru_5", "NehruUsers5")
		.ageValidatorAsOnDate(rowNumC5,dateAsOnDate2, "Nehru_5", "NehruUsers5")
		//.ageValidatorLeapyear(dateLeap, "BNP_1", "BnpUsers1")
		.clickDetailsButton();
		

	}
}








