package com.nabard.tc.copy;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ibps.pages.PersonalDetailsPage;

import lib.selenium.PreAndPost;

public class TC001_NabardBasicDetails extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC001_NabardBasicDetails";
		testDescription = "Nabard age validation";
		nodes = "Nabard";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "Nabard_1";
		sheetName = "NabardUsers1";
	}
	
	@Test(dataProvider = "fetchData")	
	public void basicDetails(String RegionalOffice, String CenterOfExam,String category, String pwd,String categoryConsider,
			String pwdType ,String compensatoryTime, String	compensatory,	
			String PWDCOMPEN, String PWDSUFFER, String PWDCOMPSENTIME, String SCRIBE, String JammuKashmir, String reMarried, 
			String region,String minority,String castCertificate, String exServiceman, String disabledExServiceman,
			String defenceService_day, String defenceService_month, String defenceService_year, String ServicePeriod, String CivilSide,
			String DependentofServicemankilled, String MatriculateExServicemencandidate, String domicileOfState, String EmployeeofNABARD,
			String empNum,
			
			String maxDob, String minDob, String dateAsOnDate, String dateAsOnDateCal, String dateAsOnDate2,
			String dateLeap, String MaxAgeMOne, String MaxAge,String MaxAgePOne, String MinAgeMOne,
			String MinAge, String MinAgePOne, String MaxAgeP45, String MinAgeM45, String leapPass, String asOnDateMaxResult,
			String asOnDateMinResult, String payamount) throws Exception {

		
		new PersonalDetailsPage(driver, test)
		
		.selectDropDownUsingIndexNew(locateElement("selregionapplied_id"), RegionalOffice)
		.selectDropDownUsingIndexNew(locateElement("IBPS_selexamcentre_id"), CenterOfExam)		
		.radioButtonId("IBPS_category_id", category) 
		.alertAccept() 
		.radioButtonId("IBPS_disablilty_id", pwd)
		.selectDropDownUsingIndexNew(locateElement("disability_category_id"), categoryConsider)
		.selectDropDownUsingIndexNew(locateElement("disabliltyType_name"), pwdType)	
		.radioButtonId("IBPS_disabilitysuffersoc1", compensatoryTime)
		.radioButtonId("IBPS_disabilitysuffersoc1", compensatory)
		.radioButtonId("IBPS_disabilitysuffersoc1", PWDCOMPEN)
		.radioButtonId("IBPS_disabilitysuffersoc1", PWDSUFFER)
		.radioButtonId("IBPS_disabilitysuffersoc1", PWDCOMPSENTIME)
		.radioButtonId("IBPS_disabilitysuffersoc1", SCRIBE)
		.radioButtonId("IBPS_disabilitysuffersoc1", JammuKashmir)
		.radioButtonId("IBPS_disabilitysuffersoc1", reMarried)
		.selectDropDownUsingIndexNew(locateElement("IBPS_religion_name"), region)
		.radioButtonId("IBPS_disabilitysuffersoc1", minority)
		.radioButtonId("IBPS_disabilitysuffersoc1", castCertificate)
		.radioButtonName("IBPS_exServiceman_name", exServiceman)
		.radioButtonId("IBPS_disabilitysuffersoc1", disabledExServiceman)
		.selectDropDownUsingIndexNew(locateElement("IBPS_disabliltyType_name"), defenceService_day)
		.selectDropDownUsingIndexNew(locateElement("IBPS_disabliltyType_name"), defenceService_month)
		.selectDropDownUsingIndexNew(locateElement("IBPS_disabliltyType_name"), defenceService_year)
		.inputTextId("IBPS_disabilitysuffersoc1", ServicePeriod)
		.radioButtonId("IBPS_disabilitysuffersoc1", CivilSide)
		.radioButtonId("IBPS_disabilitysuffersoc1", DependentofServicemankilled)
		.radioButtonId("IBPS_disabilitysuffersoc1", MatriculateExServicemencandidate)
		.radioButtonId("IBPS_disabilitysuffersoc1", domicileOfState)
		.radioButtonId("IBPS_disabilitysuffersoc1", EmployeeofNABARD)
		.inputTextId("IBPS_disabilitysuffersoc1", empNum)  
		.rowNumC1()		
		.ageValidatorMax(rowNumC1, maxDob, "Nabard_1", "NabardUsers1")
		.ageValidatorMin(rowNumC1, minDob, "Nabard_1", "NabardUsers1")
		.ageValidatorAsOnDate(rowNumC1,dateAsOnDate2, "Nabard_1", "NabardUsers1")
		//.ageValidatorLeapyear(dateLeap, "BNP_1", "BnpUsers1")
		.clickDetailsButton();
		

	}
}








