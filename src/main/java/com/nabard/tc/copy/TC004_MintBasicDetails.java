package com.nabard.tc.copy;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ibps.pages.PersonalDetailsPage;

import lib.selenium.PreAndPost;

public class TC004_MintBasicDetails extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC004_MintBasicDetails";
		testDescription = "Mint age validation";
		nodes = "Mint";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "Mint_4";
		sheetName = "MintUsers4";
	}
	
	@Test(dataProvider = "fetchData")	
	public void basicDetails(String desc, String post,String category, String categoryConsider, String pwd, String pwdType ,String cerebralPalcy, String	PalcyCompensatoryTime,	
			String dominantHand, String	dominantHandComTime, String	SCRIBE,  String region,String MINORITY,String JammuKashmir, String exServiceman, 
			String ServicePeriod, String DeptCandidate, String civilSide, String examCenter,
			
			String maxDob, String minDob, String dateAsOnDate, String dateAsOnDateCal, String dateAsOnDate2,
			String dateLeap, String MaxAgeMOne, String MaxAge,String MaxAgePOne, String MinAgeMOne,
			String MinAge, String MinAgePOne, String MaxAgeP45, String MinAgeM45, String leapPass, String asOnDateMaxResult,
			String asOnDateMinResult, String payamount) throws Exception {


		new PersonalDetailsPage(driver, test)
		
		.selectDropDownUsingIndexNew(locateElement("IBPS_post_id"), post)
		.radioButtonId("IBPS_category_id", category)
		.alertAccept()
		.radioButtonId("IBPS_appldcategory", categoryConsider)
		.radioButtonId("IBPS_disablilty_id", pwd)
		.selectDropDownUsingIndexNew(locateElement("IBPS_disabliltyType_name"), pwdType)
		.radioButtonId("IBPS_disabilitysuffersoc1", cerebralPalcy)
		.radioButtonId("compensatory_id", PalcyCompensatoryTime)
		.radioButtonName("IBPS_dominantHand_name", dominantHand)  
		.radioButtonName("IBPS_dominantHandCompensatoryTime_name", dominantHandComTime) 
		.radioButtonName("IBPS_scribeService_name", SCRIBE) 
		.selectDropDownUsingIndexNew(locateElement("IBPS_religion_name"), region)
		.radioButtonName("IBPS_religiousMinority_name", MINORITY) 
		.radioButtonName("IBPS_domiciledInJammu_name", JammuKashmir)
		.radioButtonName("IBPS_exServiceman_name", exServiceman)
		.inputTextId("IBPS_exServicePeriod_id", ServicePeriod)  
		.radioButtonId("IBPS_DepartmentalStaff_id", DeptCandidate)
		.radioButtonName("IBPS_govJobexService_name", civilSide) 
		.selectDropDownUsingIndexNew(locateElement("IBPS_selexamcentre_id"), examCenter) 
		.rowNumC4()		
		.ageValidatorMax(rowNumC4, maxDob, "Mint_4", "MintUsers4")
		.ageValidatorMin(rowNumC4, minDob, "Mint_4", "MintUsers4")
		.ageValidatorAsOnDate(rowNumC4,dateAsOnDate2, "Mint_4", "MintUsers4")
		//.ageValidatorLeapyear(dateLeap, "BNP_1", "BnpUsers1")
		.clickDetailsButton();
		

	}
}








